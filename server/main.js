const app = require('http').createServer();
const io = require('socket.io')(app);
const socket = require('./socket');

const PORT = 8040;

// maybe there is no need to listen?
app.listen(PORT, "localhost", function(error) {
  if (error) {
    console.error("Unable to listen on port", PORT, error);
    return;
  }

  socket(io);
  console.log("Server started on port 8040");
});

