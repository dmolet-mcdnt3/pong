const { Player } = require("./player");
const { Ball } = require("./ball");

/**
 * Contains all the data about the game
 */
class GameState {
    active = false;

    /** @type {Player[]} */
    _players = new Array();

    /** @type {Ball} */
    _ball = new Ball();

    getPlayerCount() {
        return this._players.length;
    }

    getPlayers() {
        return this._players;
    }

    getBall() {
        return this._ball;
    }

    /** @param {string} playerName */
    addPlayer(playerName) {
        const player = new Player(playerName);

        if (this._players.length == 0) {
            player.number = 1;
        } else {
            let existingPlayer = this._players[0];
            player.number = existingPlayer.number == 2 ? 1 : 2;
        }

        console.log("Player " + player.number + " joined");
        this._players.push(player);
        return player;
    }

    /** @param {Player} player */
    removePlayer(player) {
        const index = this._players.indexOf(player);
        if (index < 0) {
            return false;
        }

        this._players.splice(index - 1, 1);
        return true;
    }
}

module.exports = {
    GameState
}
