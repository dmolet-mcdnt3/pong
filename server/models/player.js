const { Rectangle } = require("./rectangle");
const { Point } = require("./point");

const DEFAULT_PLAYER_HEIGHT = 60;
const DEFAULT_PLAYER_WIDTH = 20;

class Player {
    height = DEFAULT_PLAYER_HEIGHT;
    width = DEFAULT_PLAYER_WIDTH;

    /** @type {string} */
    name;

    /** @type {number} */
    score = 0;

    /**
     * Player number (Player one or two)
     * @type {number}
     */
    number;

    /**
     * Position of the player (y axis only)
     * @type {Point}
     */
    pos = new Point(0, 0);

    /** @type {number} */
    movementY = 0;

    playerSpeed = 9;

    /**
     * @param {string} name
     */
    constructor(name) {
        if (!name) {
            throw new Error("Invalid name");
        }

        this.name = name;
    }

    /**
     * @param {number} x
     * @param {number} y
     */
    setPos(x, y) {
        this.pos.x = x;
        this.pos.y = y;
    }

    /**
     * @param {number} coef Speed coefficient
     */
    tick(coef) {
        if (isNaN(coef)) {
            coef = 1;
        }

        this.pos.y = this.pos.y + this.playerSpeed * this.movementY * coef;
        this.movementY = 0;
    }

    getRectangle() {
        return new Rectangle(new Point(this.pos.x, this.pos.y), this.width, this.height);
    }
}

module.exports = {
    Player,
    DEFAULT_PLAYER_HEIGHT,
    DEFAULT_PLAYER_WIDTH
};
