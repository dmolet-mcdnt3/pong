class Point {
    /** @type {number} */
    x;
    /** @type {number} */
    y;

    /**
     * @param {number} x
     * @param {number} y
     */
    constructor(x, y) {
      this.x = x;
      this.y = y;
    }

    /**
     * @param {Point} a
     * @param {Point} b
     */
    static add(a, b) {
       return new Point(
         a.x + b.x,
         a.y + b.y
       );
    }

    /**
     * @param {Point} a
     * @param {Point} b
     */
    static mult(a, b) {
      return new Point(
        a.x * b.x,
        a.y * b.y
      );
    }
}

module.exports = { Point };