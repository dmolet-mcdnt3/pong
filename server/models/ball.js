// eslint-disable-next-line no-unused-vars
const { Point } = require("./point");
const { Rectangle } = require("./rectangle");

const MAX_SPEED = 20;

class Ball {
    /** @type {Point} */
    pos = new Point(0, 0);
    /** @type {Point} */
    movementVector = new Point(0, 0);
    accel = 1.25;

    size = 20;

    bounceHorizontal() {
        this.movementVector = new Point(
            Math.min(this.movementVector.x * -1 * this.accel, MAX_SPEED),
            Math.min(this.movementVector.y * this.accel, MAX_SPEED)
        );
    }

    bounceVertical() {
        this.movementVector = new Point(
            Math.min(this.movementVector.x * this.accel, MAX_SPEED),
            Math.min(this.movementVector.y * -1 * this.accel, MAX_SPEED)
        );
    }

    /**
     * @param {number} coef Speed coefficient
     */
    tick(coef) {
        if (isNaN(coef)) {
            coef = 1;
        }

        this.pos = Point.add(this.pos, Point.mult(this.movementVector, new Point(coef, coef)));
    }

    getFuturePositionX(coef) {
        if (isNaN(coef)) {
            coef = 1;
        }

        return this.pos.x + this.movementVector.x * coef;
       }

    getFuturePositionY(coef) {
        if (isNaN(coef)) {
            coef = 1;
        }

        return this.pos.y + this.movementVector.y * coef;
    }

    getRectangle() {
        return new Rectangle(new Point(this.pos.x, this.pos.y), this.size, this.size);
    }
}

module.exports = { Ball };