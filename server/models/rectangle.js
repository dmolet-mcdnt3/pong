// eslint-disable-next-line no-unused-vars
const { Point } = require("./point");

class Rectangle {
    /** @type {Point} */
    bottomLeft;

    /** @type {number} */
    width;
    /** @type {number} */
    height;

    /**
     * @param {Point} position
     * @param {number} width
     * @param {number} height
     */
    constructor(position, width, height) {
        this.bottomLeft = position;
        this.width = width;
        this.height = height;
    }

    /** @param {Rectangle} rect */
    intersects(rect) {
        let thisRight = this.bottomLeft.x + this.width;
        let thisLeft = this.bottomLeft.x;
        let thisTop = this.bottomLeft.y + this.height;
        let thisBottom = this.bottomLeft.y;

        let rectRight = rect.bottomLeft.x + rect.width;
        let rectLeft = rect.bottomLeft.x;
        let rectTop = rect.bottomLeft.y + rect.height;
        let rectBottom = rect.bottomLeft.y;

        return thisLeft < rectRight && thisRight > rectLeft
            && thisBottom < rectTop && thisTop > rectBottom;
    }
}

module.exports = { Rectangle };