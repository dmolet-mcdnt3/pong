const { GameState } = require("./models/gameState");
// eslint-disable-next-line no-unused-vars
const { Player } = require("./models/player");
const { GameController } = require("./gameController");

const gameController = new GameController(new GameState());

/** @param {SocketIO.Socket} client */
function onConnection(client) {

    client.emit("connected", {
        players: gameController.gameState.getPlayers()
    });

    client.on("join", playerName => {
        /** @type {Player} */
        let player = null;

        let players = gameController.gameState.getPlayers();
        if (players.length >= 2) {
            client.emit("joined", null);
            return;
        }

        player = gameController.addPlayer(playerName);
        if (!player) {
            client.emit("joined", null);
            return;
        }

        client.on("disconnect", reason => {
            console.log(`${player.name} (player #${player.number}) left the game (${reason})`);
            gameController.removePlayer(player);
            client.broadcast.emit("playerLeft", { playerNo: player.number });
        });

        client.on("move", direction => {
            if (direction < 0) {
                player.movementY = -1;
            } else {
                player.movementY = 1;
            }
        });

        client.emit("joined", {
            playerNo: player.number,
            players: gameController.gameState.getPlayers()
        });

        client.broadcast.emit("playerJoined", {
            player: player
        });
    });
}

/** @param {SocketIO.Server} io */
module.exports = (io) => {
    io.on("connection", onConnection);
    gameController.events.on("tick", tickData => {
        io.emit("tick", tickData);
    });
}