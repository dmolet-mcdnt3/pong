// eslint-disable-next-line no-unused-vars
const { GameState } = require("./models/gameState");
// eslint-disable-next-line no-unused-vars
const { Player } = require("./models/player");
const { Point } = require("./models/point");
const EventEmitter = require("events");

const DEFAULT_TICKS_PER_SECOND = 60;
const DEFAULT_FIELD_WIDTH = 700;
const DEFAULT_FIELD_HEIGHT = 400;
class GameControllerEventEmitter extends EventEmitter {}

class GameController {
    /** @type {GameState} */
    gameState;
    /** @type {NodeJS.Timeout} */
    gameLoopInterval;
    ticksPerSecond = DEFAULT_TICKS_PER_SECOND;
    fieldWidth = DEFAULT_FIELD_WIDTH;
    fieldHeight = DEFAULT_FIELD_HEIGHT;

    events = new GameControllerEventEmitter();

    /** @param {GameState} gameState */
    constructor(gameState) {
        this.gameState = gameState;
    }

    start() {
        this.reset();
        this.gameLoopInterval = setInterval(this.tick.bind(this), 1000 / this.ticksPerSecond);
        this.gameState.active = true;
        console.log("Game started.");
    }

    stop() {
        clearInterval(this.gameLoopInterval);
        this.gameState.active = false;
        this.gameLoopInterval = null;
        console.log("Game stopped.");
    }

    reset() {
        this.resetBall();
        this.resetPlayerPositions();
    }

    tick() {

        for (let player of this.gameState.getPlayers()) {
            player.tick();
            if (player.pos.y > this.fieldHeight - player.height) {
                player.setPos(this.fieldHeight - player.height);
            } else if (player.pos.y < 0) {
                player.pos.y = 0;
            }
        }

        this.checkCollisionWithPlayer();

        const ball = this.gameState.getBall();
        ball.tick();

        if (ball.pos.x <= 0) {
            // hits left side
            this.goal(2);
        } else if (ball.pos.x >= this.fieldWidth - ball.size) {
            // hits right side
            this.goal(1);
        } else if (ball.pos.y <= 0 || ball.pos.y >= this.fieldHeight - ball.size) {
            // hits a wall
            ball.bounceVertical();
            ball.pos.y = ball.pos.y <= 0 ? 0 : this.fieldHeight - ball.size;
        }

        this.events.emit("tick", {
            players: this.gameState.getPlayers(),
            ball: this.gameState.getBall()
        });
    }

    goal(playerNo) {
        const player = this.gameState.getPlayers().find(p => p.number == playerNo);
        player.score++;

        const direction = playerNo == 1 ? -1 : 1;
        this.resetBall(direction);
        this.resetPlayerPositions();
    }

    resetBall(direction) {
        const ball = this.gameState.getBall();

        let upOrDown = Math.random > 0.5 ? 1 : -1;
        let angle = Math.random() * upOrDown;
        let mvt = direction > 0 ? new Point(1, angle) : new Point(-1, angle);
        const initialSpeed = 1;
        ball.movementVector = Point.mult(mvt, new Point(initialSpeed, initialSpeed));
        ball.pos = new Point(this.fieldWidth / 2 - ball.size / 2, this.fieldHeight / 2 - ball.size / 2);
    }

    resetPlayerPositions() {
        const players = this.gameState.getPlayers();
        for (let player of players) {
            player.pos.x = player.number == 1 ? 20 : this.fieldWidth - player.width - 20;
            player.pos.y = this.fieldHeight / 2 - player.height / 2;
        }
    }

    checkCollisionWithPlayer() {
        const ball = this.gameState.getBall();
        for (let player of this.gameState.getPlayers()) {
            const playerRectangle = player.getRectangle();
            const ballRectangle = ball.getRectangle();

            ballRectangle.bottomLeft.x = ball.getFuturePositionX();
            if (ballRectangle.intersects(playerRectangle)) {
                ball.bounceHorizontal();
            }

            ballRectangle.bottomLeft.y = ball.getFuturePositionY();
            if (ballRectangle.intersects(playerRectangle)) {
                ball.bounceVertical();
            }
        }
    }

    /** * @param {string} playerName */
    addPlayer(playerName) {
        if (this.gameState.getPlayerCount() >= 2) {
            return null;
        }

        const player = this.gameState.addPlayer(playerName);
        if (this.gameState.getPlayerCount() >= 2) {
            this.start();
        }
        return player;
    }

    /** @param {Player} player */
    removePlayer(player) {
        this.gameState.removePlayer(player);
        if (this.gameState.getPlayerCount() < 2){
            this.stop();
        }
    }
}

module.exports = { GameController };