import { Point } from './Point';

export interface Player {
  height : number;
  width : number;
  name : string;
  score : string;
  number : number;
  pos : Point;
}
