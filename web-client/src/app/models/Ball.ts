import { Point } from './Point';

export interface Ball {
  pos : Point;
  movementVector : Point;
  size : number;
}
