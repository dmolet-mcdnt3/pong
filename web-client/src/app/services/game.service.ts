import { Injectable } from '@angular/core';
import { connect as io } from 'socket.io-client';
import { Player } from '../models/Player';
import { Ball } from '../models/Ball';
import { BehaviorSubject, Subject } from 'rxjs';

export interface TickData {
  players : Player[];
  ball : Ball;
}

@Injectable({
  providedIn: 'root'
})
export class GameService {
  playerNo : number;
  playerName : string;
  players : Player[];
  ball : Ball;

  players$ = new BehaviorSubject<Player[]>([]);
  playerJoined$ = new Subject<Player>();
  tick$ = new Subject<TickData>();

  private socket : SocketIOClient.Socket;

  constructor() {
    this.socket = io("localhost:8040", { transports: ['websocket'], upgrade: false, autoConnect: false });
    this.socket.on("connected", this.onConnected.bind(this));
    this.socket.on("joined", this.onJoined.bind(this));
    this.socket.on("tick", this.onTick.bind(this));
    this.socket.on("playerJoined", this.onPlayerJoined.bind(this));
    this.socket.on("playerLeft", this.onPlayerLeft.bind(this));
    this.socket.on("disconnect", this.onDisconnect.bind(this));
    this.socket.on("reconnect", this.onReconnect.bind(this));

    document.addEventListener('onbeforeunload', () => {
      if (this.socket) {
        this.socket.close();
      }
    });
  }

  get isConnected() : boolean {
    return this.socket && this.socket.connected;
  }

  connect() {
    return new Promise((resolve, reject) => {
      this.socket.once("connected", () => resolve());
      this.socket.once("error", e => reject(e));
      this.socket.connect();
    });
  }

  disconnect() {
    this.socket.disconnect();
    this.playerNo = null;
  }

  join(playerName : string) {
    this.socket.emit("join", playerName);
    this.playerName = playerName;
  }

  move(direction) {
    this.socket.emit("move", direction);
  }

  private onConnected(data : { players : Player[] }) {
    console.log("connected", data);
    this.setPlayers(data.players);
  }

  private onDisconnect() {
    console.log("disconnect");
    this.setPlayers([]);
    this.playerNo = 0;
  }

  private onReconnect() {
    this.join(this.playerName);
  }

  private onJoined(data : { playerNo : number, players : Player[] }) {
    console.log("joined", data);
    if (!data) {
      return;
    }

    this.playerNo = data.playerNo;
    this.setPlayers(data.players);
  }

  private onTick(data : TickData) {
    this.ball = data.ball;
    this.setPlayers(data.players);
    this.tick$.next(data);
  }

  private onPlayerJoined(data : { player : Player }) {
    console.log("playerJoined", data);
    this.playerJoined$.next(data.player);
  }

  private onPlayerLeft(data : { playerNo : number }) {
    this.setPlayers(this.players.filter(x => x.number != data.playerNo));
  }

  private setPlayers(players : Player[]) {
    this.players = players;
    this.players$.next(players);
  }
}
