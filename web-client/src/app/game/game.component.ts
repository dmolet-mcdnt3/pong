import { Component, OnInit, ChangeDetectionStrategy, HostListener } from '@angular/core';
import { GameService, TickData } from '../services/game.service';
import { Player } from '../models/Player';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { Ball } from '../models/Ball';

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class GameComponent implements OnInit {
  destroy$ = new Subject();

  players : Player[] = new Array(2);

  get player1() { return this.players[0]; }
  get player2() { return this.players[1]; }

  ball : Ball;

  constructor(private gameService : GameService) { }

  get playerNo() {
    return this.gameService.playerNo;
  }

  ngOnInit() {
    this.gameService.playerJoined$.pipe(
      takeUntil(this.destroy$)
    ).subscribe(this.onPlayerJoined.bind(this))

    this.gameService.tick$.pipe(
      takeUntil(this.destroy$)
    ).subscribe(this.onTick.bind(this));

    this.gameService.players$.pipe(
      takeUntil(this.destroy$)
    ).subscribe(players => this.players = players);

    if (!this.gameService.isConnected) {
      this.gameService.connect().then(() => {
        this.gameService.join("Player " + (Math.random() * 100000000).toFixed());
      });
    }
  }

  @HostListener('document:keydown', ['$event'])
  onKeyUp(e : KeyboardEvent) {
    if (!this.gameService.playerNo) {
      return;
    }

    switch (e.keyCode) {
      case 38: // Arrow up
      case 87: // W
      case 90: // Z
        this.gameService.move(1);
      break;
      case 40: // Arrow down
      case 83: // S
        this.gameService.move(-1);
      break;
    }
  }

  onTick(data : TickData) {
    this.ball = data.ball;
  }

  onPlayerJoined(player) {
    this.players[player.number - 1] = player;
    console.log(this.players);
  }

  ngOnDestroy() {
    this.destroy$.unsubscribe();
    this.destroy$.next();
  }
}
