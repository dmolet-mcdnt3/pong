import { Component, Input, HostBinding } from '@angular/core';
import { Point } from 'src/app/models/Point';

@Component({
  selector: 'app-ball',
  templateUrl: './ball.component.html',
  styleUrls: ['./ball.component.scss']
})
export class BallComponent {

  @Input() position : Point;
  @Input() size : number = 20;

  @HostBinding('style.left')
  get leftPx() { return this.position.x + 'px'; }
  @HostBinding('style.bottom')
  get bottomPx() { return this.position.y + 'px'; }
  @HostBinding('style.width')
  get widthPx() { return this.size + 'px'; }
  @HostBinding('style.height')
  get heightPx() { return this.size + 'px'; }
}
