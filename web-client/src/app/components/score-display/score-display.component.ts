import { Component, OnInit, Input } from '@angular/core';
import { ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-score-display',
  templateUrl: './score-display.component.html',
  styleUrls: ['./score-display.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class ScoreDisplayComponent implements OnInit {
  @Input() score : number = 0;

  constructor() { }

  ngOnInit() {
  }

}
