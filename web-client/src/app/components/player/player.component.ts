import { Component, OnInit, Input, ChangeDetectionStrategy, HostBinding } from '@angular/core';

@Component({
  selector: 'app-player',
  templateUrl: './player.component.html',
  styleUrls: ['./player.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PlayerComponent {
  @Input() x : number = 0;
  @Input() y : number = 0;

  @Input() height : number;
  @Input() width : number;

  @HostBinding('style.left')
  get leftPx() { return this.x + 'px'; }
  @HostBinding('style.bottom')
  get bottomPx() { return this.y + 'px'; }
  @HostBinding('style.width')
  get widthPx() { return this.width + 'px'; }
  @HostBinding('style.height')
  get heightPx() { return this.height + 'px'; }
}
